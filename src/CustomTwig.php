<?php

namespace Drupal\simple_forum;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Custom twig functions.
 */
class CustomTwig extends AbstractExtension {

  /**
   *
   */
  public function getFunctions() {
    return [
      new TwigFunction('getCommentRating', [$this, 'getCommentRating']),
    ];
  }

  /**
   *
   */
  public function getCommentRating($type, $commentid, $userid = NULL, $rate = NULL) {
    $output = '';
    $database = \Drupal::database();
    if ($type == 'usercommentrate') {
      $query = $database->select('simple_forum_comments_rating', 'sfcr');
      $query->condition('sfcr.cid', $commentid, '=');
      $query->condition('sfcr.uid', $userid, '=');
      $query->condition('sfcr.rating', $rate, '=');
      $query->fields('sfcr', ['uid', 'rating']);
      $resultuserrate = $query->execute()->fetchAssoc();
      if (!empty($resultuserrate)) {
        $output = 'checked';
      }
      else {
        $output = '';
      }
    }
    elseif ($type == 'commentrate') {
      $query = $database->select('simple_forum_comments_rating', 'sfcr');
      $query->condition('sfcr.cid', $commentid, '=');
      $query->addExpression('sum(rating)', 'totalrating');
      $query->addExpression('count(uid)', 'countuser');
      $resultrate = $query->execute()->fetchObject();
      if (!empty($resultrate) && !empty($resultrate->totalrating) && $resultrate->countuser > 0) {
        $output = $resultrate->totalrating / $resultrate->countuser;
      }
      else {
        $output = 0;
      }
    }
    return $output;
  }

}
