<?php

namespace Drupal\simple_forum\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 *
 */
class SimpleForumController extends ControllerBase {

  /**
   * Save/update rating in custom table.
   */
  public function Rating(Request $request) {
    $output        = [];
    $datauserid    = $request->request->get('data_userid');
    $datanodeid    = $request->request->get('data_nodeid');
    $datacommentid = $request->request->get('data_commentid');
    $datarating    = $request->request->get('data_rating');
    if ($datauserid) {
      /** @var \Drupal\Core\Database\Connection $connection */
      $connection = \Drupal::service('database');
      $connection->merge('simple_forum_comments_rating')

        ->key('uid', $datauserid)
        ->key('cid', $datacommentid)
        ->fields([
          'uid' => $datauserid,
          'nid' => $datanodeid,
          'cid' => $datacommentid,
          'rating' => $datarating,
          'rating_date' => \Drupal::time()->getRequestTime(),
        ])
        ->execute();
      $output = ['status' => 200, 'datarating' => $datarating];
    }
    return new JsonResponse($output);
  }

  /**
   * Get rating form database.
   */
  public function get_ratings($opt, $fname, $marks) {
    $res = [];
    $connection = \Drupal::service('database');
    if ($opt == "All") {
      $results = $connection->select('simple_forum_comments_rating', 'st')
        ->extend('\Drupal\Core\Database\Query\PagerSelectExtender')
        ->limit(15);
      $results->fields('st');
      $results->orderBy('st.id', 'DESC');
      $res = $results->execute()->fetchAll();
      $ret = [];
    }
    else {
      $results = $connection->select('simple_forum_comments_rating', 'st')
        ->extend('\Drupal\Core\Database\Query\PagerSelectExtender')
        ->limit(15);
      $results->fields('st');
      $results->orderBy('st.id', 'DESC');
      // $results->condition('fname', $fname);
      // $results->condition('marks', $marks);
      $res = $results->execute()->fetchAll();
      $ret = [];
    }
    foreach ($res as $row) {

      // $delete = Url::fromUserInput('/admin/structure/simple_forum/rating/delete/' . $row->id, array('attributes' => array('onclick' => "return confirm('Are you Sure')")));
      // $edit = Url::fromUserInput('/admin/structure/simple_forum/rating/edit/' . $row->id);
      // $edit_link = \Drupal::l('edit', $edit);
      // $delete_link = \Drupal::l('delete', $delete);
      // $mainLink = t('@linkApprove  @linkReject', array('@linkApprove' => $edit_link, '@linkReject' => $delete_link));
      $ret[] = [
        'id' => $row->id,
        'nid' => $row->nid,
        'uid' => $row->uid,
        'cid' => $row->cid,
        'rating' => $row->rating,
      ];
    }
    return $ret;
  }

  /**
   * Rating listing.
   */
  public function RatingListing() {
    // Get parameter value while submitting filter form.
    $fname = \Drupal::request()->query->get('fname');
    $marks = \Drupal::request()->query->get('marks');
    // Create table header.
    $header = [
      'id' => $this->t('Id'),
      'nid' => $this->t('Node Id'),
      'uid' => $this->t('User Id'),
      'cid' => $this->t('Comment Id'),
      'rating' => $this->t('Rating'),
    ];

    if ($fname == "" && $marks == "") {
      $form['table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $this->get_ratings("All", "", ""),
        '#empty' => $this->t('No users found'),
      ];
    }
    else {
      $form['table'] = [
        '#type' => 'table',
        '#header' => $header,
        '#rows' => $this->get_ratings("", $fname, $marks),
        '#empty' => $this->t('No records found'),
      ];
    }
    $form['pager'] = [
      '#type' => 'pager',
    ];
    return $form;
  }

}
