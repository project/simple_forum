# simple_forum

INTRODUCTION
------------

Simple Forum module is used for create a forum and on in create comments.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/simple_forum

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/simple_forum


INSTALLATION
--------------------------

Note: Installation is very basic download and install the module and find the link "New Simple Forum Topic" under the "admin/structure" menu.
Create a node forum with the default category "General".

This module are provide give to  star rating(1 to 5) on comments by given user and provide overall rating mention on comments.

To install Introspect module, follow the steps mentioned below.

 - Module file can be downloaded from the link
   https://www.drupal.org/project/simple_forum
 - Extract the downloaded file to the modules directory.
 - Goto Extend, find Simple Forum module and choose 'Install'.
 - You have now enabled your module.
