jQuery(document).ready(function(){
	
	jQuery(document).on('click','.comment-rating',function () {
		var userid = jQuery(this).attr('userid');
		var nodeid = jQuery(this).attr('nodeid');
		var commentid = jQuery(this).attr('commentid');
		var rating = jQuery(this).attr('rating');
		jQuery.ajax({
      url: drupalSettings.simpleforum.site_url+'comment/rating',
      type: 'POST',
      data: {data_userid: userid,data_nodeid:nodeid,data_commentid:commentid, data_rating:rating},
      dataType: 'json',
      beforeSend: function() {
      },
      success: function(response) {
        
      },
    });
	});
});